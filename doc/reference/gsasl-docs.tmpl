<!doctype book PUBLIC "-//Davenport//DTD DocBook V3.0//EN" [
<!ENTITY gsasl-gsasl SYSTEM "gsasl.sgml">
<!ENTITY gsasl-gsasl-mech SYSTEM "gsasl-mech.sgml">
<!ENTITY gsasl-gsasl-compat SYSTEM "gsasl-compat.sgml">
]>
<book id="index">
  <bookinfo>
    <title>GNU SASL API Reference Manual</title>
  </bookinfo>

  <chapter>
    <title>GNU SASL API Reference Manual</title>

    <para>
GNU SASL is an implementation of the Simple Authentication and
Security Layer framework and a few common SASL mechanisms.  SASL is
used by network servers (e.g., IMAP, SMTP) to request authentication
from clients, and in clients to authenticate against servers.
    </para>
    <para>
GNU SASL consists of a library (`libgsasl'), a command line utility
(`gsasl') to access the library from the shell, and a manual.  The
library includes support for the framework (with authentication
functions and application data privacy and integrity functions) and at
least partial support for the CRAM-MD5, EXTERNAL, GSSAPI, ANONYMOUS,
PLAIN, SECURID, DIGEST-MD5, LOGIN, and NTLM mechanisms.
    </para>
    <para>
The library is easily ported because it does not do network
communication by itself, but rather leaves it up to the calling
application.  The library is flexible with regards to the
authorization infrastructure used, as it utilize a callback into the
application to decide whether a user is authorized or not.
    </para>
    <para>
GNU SASL is developed for the GNU/Linux system, but runs on over 20
platforms including most major Unix platforms and Windows, and many
kind of devices including iPAQ handhelds and S/390 mainframes.
    </para>
    <para>
GNU SASL is written in pure ANSI C89 to be portable to embedded and
otherwise limited platforms.  The entire library, with full support
for ANONYMOUS, EXTERNAL, PLAIN, LOGIN and CRAM-MD5, and the front-end
that support client and server mode, and the IMAP and SMTP protocols,
fits in under 60kb on an Intel x86 platform, without any modifications
to the code.  (This figure was accurate as of version 0.0.13.)
    </para>
    <para>
The library is licensed under the GNU Lesser General Public License
version 2.1.  The command-line application (src/), examples
(examples/), self-test suite (tests/) are licensed under the GNU
General Public License license version 3.0.  The documentation (doc/)
is licensed under the GNU Free Documentation License version 1.2.
    </para>

    &gsasl-gsasl;
    &gsasl-gsasl-mech;
    &gsasl-gsasl-compat;
  </chapter>
</book>
